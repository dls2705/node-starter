const express = require('express');
const router = express.Router();

const ExamplesController = require('../controllers/examples');

router.get('/', ExamplesController.examples_get_all);

module.exports = router;