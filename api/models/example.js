function Example(id, relatedItem, quantity) {
    var example = {};

    example.id = id;
    example.relatedItem = relatedItem;
    example.quantity = quantity;

    return example;
}

module.exports = Example;

/*
Uncomment if you are using mongoDB

const mongoose = require('mongoose');

const exampleSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    relatedItem: { type: mongoose.Schema.Types.ObjectId, ref: 'RelatedItem' },
    quantity: { type: Number, default: 1 }
});

module.exports = mongoose.model('Example', exampleSchema);
*/