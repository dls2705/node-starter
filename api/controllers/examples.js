const Example = require('../models/example');

exports.examples_get_all = (req, res, next) => {

    const examples = [
        new Example(1, 'FakeRelatedItem', 4),
        new Example(2, 'FakeRelatedItem', 5),
        new Example(3, 'FakeRelatedItem', 56),
    ];
    
    res.status(200).json(examples);
};