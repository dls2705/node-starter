# Node Js - Starter Project

This project was generated with Npm

## Topics covered in this document

* [Getting Started](#getting_started)

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

* Node (Latest Version, LTS recommended)
    * [nvm recommended](https://github.com/creationix/nvm/blob/master/README.md)
* MongoDB
    * [mongoDB](https://www.mongodb.com/)
* Git (recommended) :
    * [commitizen](https://github.com/commitizen/cz-cli)
    * [gitflow](https://danielkummer.github.io/git-flow-cheatsheet/)

### Installing


``` bash
# Clone project
git clone https://gitlab.com/dls2705/node-starter.git
```
``` bash
# Create develop branch
cd node-starter
git checkout -b develop
git pull origin develop
```

``` bash
# Install dependencies
cd node-starter
npm install
```

``` bash
# Run (mongoDB previously started)
npm start
```